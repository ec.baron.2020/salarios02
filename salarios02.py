class Empleado: 
    def __init__ (self, n, s):
        self.nombre = n
        self.nomina = s

    def calculo (self):
        return self.nomina * 0.30
    
    def __str__(self):
        return "El empleado {name} debe pagar {tax:.2f}".format (name = self.nombre, tax=self.calculo())


empleadoPepe = Empleado ("Pepe", 20000)
empleadoAna = Empleado ("Ana", 30000) 

total = empleadoPepe.calculo() + empleadoAna.calculo()

print (empleadoPepe) 
print (empleadoAna)
print ("Los impuestos a pagar en total son {:.2f} euros".format (total))



    



